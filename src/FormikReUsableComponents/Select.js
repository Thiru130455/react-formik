import { ErrorMessage, Field } from 'formik';
import React from 'react'
import TextError from './TextError';

function Select(props) {
    const { lable, name, options, ...rest } = props;
    return (
        <div className="form-cotrol">
            <label htmlFor={name}>{lable}</label>
            <Field as="select" name={name} {...rest}>
                {options.map(option => {
                    return (
                        <option key={option.value} value={option.value}>
                            {option.key}
                        </option>
                    )
                })}
            </Field>
            <ErrorMessage name={name} component={TextError} />
        </div>
    )
}

export default Select

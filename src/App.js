import React from 'react';
import './App.css';
import YouTubeForm from './Components/YouTubeForm';
import FormikContainer from './FormikReUsableComponents/FormikContainer';
import Login from './FormikReUsableComponents/Login';
import RegistrationForm from './FormikReUsableComponents/RegistrationForm';
import CourseEnrollmentForm from './FormikReUsableComponents/CourseEnrollmentForm';
import { theme, ThemeProvider } from "@chakra-ui/core";
import ChakraLogin from './FormikReUsableComponents/Chakra-ui/ChakraLogin';

function App() {
  return (
    // <ThemeProvider theme={theme}>

    <div className="App">
      {/* <YouTubeForm/> */}
      {/* <FormikContainer/> */}
      {/* <Login/> */}
      {/* <RegistrationForm/> */}
      <CourseEnrollmentForm />
      {/* <ChakraLogin /> */}
    </div>
    // </ThemeProvider>
  );
}

export default App;

import React from 'react'
import TextError from './TextError';
import { ErrorMessage, Field } from 'formik'

function TextArea(props) {
    const { label, name, ...rest } = props
    return (
        <div className="form-cotrol">
            <label htmlFor={name}>{label}</label>
            <Field as="textarea" id={name} name={name} {...rest} />
            <ErrorMessage name={name} component={TextError} />

        </div>
    )
}

export default TextArea

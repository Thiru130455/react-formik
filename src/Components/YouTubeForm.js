import React, { useState } from 'react'
import { Formik, Field, Form, ErrorMessage, FieldArray, FastField } from 'formik';
import * as Yup from 'yup';
import TextError from './TextError';
const initialValues = {
    name: 'Thiru',
    email: '',
    channel: '',
    comments: '',
    address: '',
    social: {
        facebook: '',
        twitter: ''
    },
    phoneNumbers: ['', ''],
    phNumbers: ['']
}
const savedValues = {
    name: 'Thiru',
    email: 'thiru@gmail.com',
    channel: 'All katti viral',
    comments: 'welcome to formik',
    address: '',
    social: {
        facebook: '',
        twitter: ''
    },
    phoneNumbers: ['', ''],
    phNumbers: ['']
}
const onSubmit = (values, submitting) => {
    console.log(values);
    submitting.setSubmitting(false);
    submitting.resetForm();

}

const validateComments = value => {
    let error
    if (!value) {
        error = 'Required';
    }
    return error;
}


function YouTubeForm() {
    const validationSchema = Yup.object({
        name: Yup.string().required('Required'),
        email: Yup.string().email('Invalid email format')
            .required('Required'),
        channel: Yup.string().required('Required'),
        // address: Yup.string().required('Required')
    })

    const [formValue, setformValue] = useState(null)
    return (
        <Formik
            initialValues={formValue || initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
            validateOnMount
            enableReinitialize
        // validateOnChange={false}
        // validateOnBlur={false}
        >
            {(formik) => {
                console.log(formik);

                return (<Form>
                    <div className="form-control">
                        <label htmlFor="name">Name</label>
                        <Field type="text" id="name" name="name" />
                        <ErrorMessage name="name" component={TextError} />
                    </div>

                    <div className="form-control">
                        <label htmlFor="email">Email</label>
                        <Field type="email" id="email" name="email" />
                        <ErrorMessage name="email" />
                    </div>

                    <div className="form-control">
                        <label htmlFor="channel">Channel</label>
                        <Field type="text" id="channel" name="channel" />
                        <ErrorMessage name="channel" />
                    </div>
                    <div className="form-control">
                        <label htmlFor="comments">Comments</label>
                        <Field as="textarea" id="comments" placeholder="comments..." name="comments"
                            validate={validateComments} />
                        <ErrorMessage name="comments" component={TextError} />
                    </div>

                    <div className="form-control">
                        <label htmlFor="address">Address</label>
                        <FastField name="address" >
                            {(props) => {
                                console.log('Field render');
                                const { meta, form, field, } = props;
                                return (<div>
                                    <input type="text" id="address" {...field} />
                                    {meta.touched && meta.error ? (<div>{meta.error}</div>) : null}
                                </div>)
                            }}
                        </FastField>
                    </div>
                    <div className="form-control">
                        <label htmlFor="facebook">Facebook profile</label>
                        <Field type="text" id="facebook" placeholder="facebook..." name="social.facebook" />
                    </div>
                    <div className="form-control">
                        <label htmlFor="twitter">Twitter profile</label>
                        <Field type="text" id="twitter" placeholder="twitter..." name="social.twitter" />
                    </div>

                    <div className="form-control">
                        <label htmlFor="primaryPh">Primay phone number</label>
                        <Field type="text" id="primaryPh" placeholder="primary" name="phoneNumbers[0]" />
                    </div>
                    <div className="form-control">
                        <label htmlFor="secondaryPh">Secondary phone number</label>
                        <Field type="text" id="secondaryPh" placeholder="secondary" name="phoneNumbers[1]" />
                    </div>
                    <div className="form-control">
                        <label >List of phone numbers</label>
                        <FieldArray name="phNumbers" >
                            {(fieldArrayProps) => {
                                const { push, remove, form } = fieldArrayProps;
                                const { values } = form;
                                const { phNumbers } = values;
                                // console.log('fieldArrayProps', fieldArrayProps);
                                return (
                                    <div>
                                        {phNumbers.map((phNumber, index) => (

                                            <div key={index}>
                                                <Field name={`phNumbers[${index}]`} />
                                                { index > 0 && (<button type="button" onClick={() => remove(index)}>
                                                    {' '}-{' '}</button>)
                                                }
                                                <button type="button" onClick={() => push('')}> {' '}+{' '}</button>

                                            </div>
                                        ))}
                                    </div>
                                )
                            }}
                        </FieldArray>

                    </div>

                    {/* <button type="button" onClick={() => formik.validateField('comments')}>validateComments</button>
                    <button type="button" onClick={() => formik.validateForm()}>validate all</button>
                    <button type="button" onClick={() => formik.setFieldTouched('comments')}>visit comments</button>
                    <button type="button" onClick={() => formik.setTouched({
                        name: true,
                        email: true,
                        channel: true,
                        comments: true
                    })}>visit all</button> */}

                    <button type="button" onClick={()=>setformValue(savedValues)}>Load saved data</button>
                    <button type="reset" >Reset</button>

                    <button type="submit" disabled={!formik.isValid || formik.isSubmitting}>Submit</button>
                </Form>)
            }}
        </Formik>
    )
}

export default YouTubeForm
